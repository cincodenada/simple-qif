Simple Bank JSON -> QIF exporter
================================

This is just a very basic exporter that will take a Simple JSON
export, and convert it into a QIF suitable for import into budgeting
programs - in my case, You Need a Budget (YNAB).  The main fancy thing
it does is export tips/cashback/etc as split transactions, because
that's what I wanted.  That's also why I couldn't just use CSV.

I went with QIF even though it's really old, because it works fine and
is what I could find a python library for.  It just generates `outfile.qif`.

For the most basic usage, just run it with the JSON file as the parameter:

    ./simple_qif.py 2015-01-01-exported_transactions.json

You can also specify a start and end date with `-s` and `-e`, which
will limit the range exported from the JSON, and specify the category
to be applied with `-c`.

A more complex example:

    ./simple_qif.py 2015-01-01-exported_transactions.json \
        -s 2014-12-01 -c "Food 'n' Stuff"

Will export only transactions on or after December 1, 2014, and give
them a catgeory of "Food 'n' Stuff".

That's it, really.  Put in a Simple JSON, get out a QIF.

License
-------
Copyright © 2015 Joel Bradshaw <cincodenada@gmail.com>

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.
