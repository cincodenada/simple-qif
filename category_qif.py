#!/usr/bin/python

from qifparse import qif
from datetime import datetime
import codecs
import argparse
import os
import csv
import re

class InputError(Exception):
    pass

def fmtstr(jsonstr):
    return jsonstr
    if jsonstr:
        return jsonstr.encode('utf8','replace')
    else:
        return ''

def fmtmoney(instr):
    return float(re.sub('[^0-9\.\-]', '', instr))

p = argparse.ArgumentParser()
p.add_argument('file', type=argparse.FileType('r'))
p.add_argument('-s','--start_date',dest='start',type=str,help='Earliest date to include in export')
p.add_argument('-e','--end_date',dest='end',type=str,help='Latest date to include in export')
p.add_argument('-c','--category',dest='cat',type=str,help='Category string to use for the exported transactions',default='Imported')
p.add_argument('-o','--open',action='store_true',help='Opens the file afterward with xdg-open')
args = p.parse_args()

mindate = datetime.strptime(args.start,'%Y-%m-%d') if args.start else None
maxdate = datetime.strptime(args.end,'%Y-%m-%d') if args.end else None
outdate = "%d/%m/%Y"

def getAmount(row):
    inflow = fmtmoney(row['Inflow'])
    outflow = fmtmoney(row['Outflow'])
    if inflow == 0 and outflow == 0:
        raise InputError("No amount found!")
    elif inflow == 0:
        return outflow
    elif outflow == 0:
        return inflow
    else:
        raise InputError(f"Found inflow and outflow ({inflow}, {outflow})!")

class QifParser:
    def __init__(self):
        self.curSplits = []
        self.splitPayee = None
        self.accounts = {}
        self.outfiles = {}
        self.cats = set()

    def finalizeSplit(self, tdate):
        total = sum([s.amount for s in self.curSplits])
        trans = qif.Transaction(
            date=tdate,
            date_format="%m/%d/%Y",
            amount=total,
            payee=self.splitPayee,
        )
        trans.splits = self.curSplits
        self.curSplits = []
        self.splitPayee = None
        return trans

    def getAccount(self, acctName):
        if acctName not in self.accounts:
            self.accounts[acctName] = qif.Account(name=acctName,account_type="Bank")
        return self.accounts[acctName]

    def getOutfile(self, acctName):
        if acctName not in self.outfiles:
            newFile = qif.Qif()
            newFile.add_account(self.getAccount(acctName))
            self.outfiles[acctName] = newFile
        return self.outfiles[acctName]

    def getPayeeOrAccount(self, row):
        transferInfo = re.match("Transfer : (.*)", row["Payee"])
        if transferInfo:
            (acctName,) = transferInfo.groups()
            return (None, self.getAccount(acctName))
        else:
            return (row["Payee"], None)

    def parseRow(self, row):
        acctName = row['Account']
        outfile = self.getOutfile(acctName)

        tdate = datetime.strptime(row['Date'], "%m/%d/%Y");
        if(mindate and tdate < mindate):
            return
        if(maxdate and tdate > maxdate):
            return

        cat = fmtstr(row['Category Group/Category'])
        self.cats.add(cat)

        amount = getAmount(row)
        (payee, to_account) = self.getPayeeOrAccount(row)
        memo = row['Memo']

        splitInfo = re.match("Split \((\d)/(\d)\) ?(.*)", row['Memo'])
        if splitInfo:
            (cur, total, memo) = splitInfo.groups()

            if(cur == 1 and self.curSplits.length != 0):
                print("Warning, discarding orphan split!")
                print(self.curSplits)
                self.curSplits = []

            if payee and not self.splitPayee:
                self.splitPayee = payee

            split = qif.AmountSplit(
                amount=amount,
                category=cat,
                to_account=to_account,
                memo=memo,
            )
            self.curSplits.append(split)
            if cur == total:
                outfile.add_transaction(self.finalizeSplit(tdate), header="!Type:Bank")
        else:
            trans = qif.Transaction(
                date=tdate,
                date_format="%m/%d/%Y",
                amount=amount,
                payee=payee,
                to_account=to_account,
                memo=memo,
                category=cat
            )
            outfile.add_transaction(trans, header="!Type:Bank")

    def writeFiles(self):
        qifCats = [qif.Category(name = cat) for cat in self.cats if cat]
        for (acctName, outfile) in self.outfiles.items():
            for cat in qifCats:
                outfile.add_category(cat)
            filename = re.sub('[^a-zA-Z \-_]', '_', acctName)
            outh = open(f'{filename}.qif','w')
            outh.write(str(outfile))
            outh.close()

with args.file as infile:
    csvfile = csv.reader(infile)
    header = next(csvfile)
    parser = QifParser()
    for (idx, bare_row) in enumerate(csvfile):
        row = dict(zip(header, bare_row))
        try:
            parser.parseRow(row)
        except InputError as e:
            print(f"Skipping row {idx}, {e}")
            print(row)

parser.writeFiles()
