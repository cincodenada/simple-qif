#!/usr/bin/python2

from qifparse import qif
import json
from datetime import datetime
import codecs
import argparse
import os

outfile = qif.Qif()

acc = qif.Account(name="Simple Export",account_type="Bank")
outfile.add_account(acc)

def fmtstr(jsonstr):
    if jsonstr:
        return jsonstr.encode('utf8','replace')
    else:
        return ''

p = argparse.ArgumentParser()
p.add_argument('file', type=argparse.FileType('r'))
p.add_argument('-s','--start_date',dest='start',type=str,help='Earliest date to include in export')
p.add_argument('-e','--end_date',dest='end',type=str,help='Latest date to include in export')
p.add_argument('-c','--category',dest='cat',type=str,help='Category string to use for the exported transactions',default='Imported')
p.add_argument('-o','--open',action='store_true',help='Opens the file afterward with xdg-open')
args = p.parse_args()

cat = qif.Category(name=args.cat)
outfile.add_category(cat)

mindate = datetime.strptime(args.start,'%Y-%m-%d') if args.start else None
maxdate = datetime.strptime(args.end,'%Y-%m-%d') if args.end else None
outdate = "%d/%m/%Y"

with args.file as infile:
    records = json.load(infile)
    for row in records['transactions']:
        # Skip holds, they're not useful
        if row['record_type'] == 'HOLD':
            continue;

        tdate = datetime.fromtimestamp(row['times']['when_recorded']/1000.0)
        if(mindate and tdate < mindate):
            continue
        if(maxdate and tdate > maxdate):
            continue

        amt = row['amounts']['amount']/10000.0
        sign = -1 if row['bookkeeping_type'] == 'debit' else 1
        curtrans = qif.Transaction(
            date=tdate,
            date_format="%m/%d/%Y",
            amount=amt*sign,
            payee=fmtstr(row['description']),
            memo=fmtstr(row['memo']),
            category=args.cat
        )
        # Check if we have splits to do
        if('amount' in row['amounts'] and 'base' in row['amounts']
                and row['amounts']['base'] != row['amounts']['amount']):
            for atype in row['amounts']:
                if(atype not in ['amount', 'cleared'] and row['amounts'][atype]):
                    split = qif.AmountSplit(
                        amount=row['amounts'][atype]/10000.0*sign,
                        category=fmtstr(args.cat)
                    )
                    curtrans.splits.append(split)

        outfile.add_transaction(curtrans, header="!Type:Bank")

outh = open('outfile.qif','w')
outh.write(str(outfile))
outh.close()

if(args.open):
    os.system("xdg-open outfile.qif")
